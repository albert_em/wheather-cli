import chalk from 'chalk';
import dedent from 'dedent-js';

import { getIcon } from './api.service.js';

const printError = (error) => {
  console.log(chalk.bgRed('ERROR') + ' ' + error);
};

const printSuccess = (message) => {
  console.log(chalk.bgGreen('SUCCESS') + ' ' + message);
};

const printHelp = () => {
  console.log(
    dedent(`${chalk.bgCyan(' HELP ')}
	Without parameters - output weather
	-s [CITY] to set city
	-h for help 
	-t [APY_KEY] to save token
	`)
  );
};

const printWeather = (res) => {
  const icon = getIcon(res.weather[0].icon);
  console.log(
    dedent(`${chalk.bgYellowBright(' WEATHER ')} Weather in the ${res.name}
	${icon} ${res.weather[0].description}
	Temperature: ${res.main.temp} C
	Feels like: ${res.main.feels_like} C
	Humidity: ${res.main.humidity}%
	Wind speed: ${res.wind.speed} mps
	`)
  );
};

export { printError, printSuccess, printHelp, printWeather };
