import { homedir } from 'os';
import { join } from 'path';
import { promises } from 'fs';

const FILE_PATH = join(homedir(), 'weather-data.json');

const DICTIONARY = {
  token: 'token',
  city: 'city',
};

const isExist = async (path) => {
  try {
    return Boolean(await promises.stat(path));
  } catch (error) {
    return false;
  }
};

const getKeyValue = async (key) => {
  if (await isExist(FILE_PATH)) {
    const file = await promises.readFile(FILE_PATH);
    const data = JSON.parse(file);
    return data[key];
  }
};

const saveKeyValue = async (key, value) => {
  let data = {};

  if (await isExist(FILE_PATH)) {
    const file = await promises.readFile(FILE_PATH);
    data = JSON.parse(file);
  }
  data[key] = value;
  await promises.writeFile(FILE_PATH, JSON.stringify(data));
};

export { saveKeyValue, getKeyValue, DICTIONARY };
