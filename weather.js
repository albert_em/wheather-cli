#!usr/bin/env node

import { getArgs } from './helpers/args.js';
import { getWeather } from './services/api.service.js';
import { printError, printHelp, printSuccess, printWeather } from './services/log.service.js';
import { DICTIONARY, getKeyValue, saveKeyValue } from './services/storage.service.js';

const saveToken = async (token) => {
  if (!token.length) {
    printError('There is no token');
    return;
  }
  try {
    await saveKeyValue(DICTIONARY.token, token);
    printSuccess('Token saved');
  } catch (error) {
    printError(error.message);
  }
};

const saveCity = async (city) => {
  if (!city.length) {
    printError('Enter valid city name');
  }
  try {
    await saveKeyValue(DICTIONARY.city, city);
    printSuccess(`${city} saved`);
  } catch (e) {
    printError(e.message);
  }
};

const getForecast = async () => {
  try {
    const city = await getKeyValue(DICTIONARY.city);
    const weather = await getWeather(process.env.CITY ?? city);
    printWeather(weather);
  } catch (e) {
    if (e?.response?.status === 404) {
      printError('Wrong city');
    } else if (e?.response?.status === 401) {
      printError('Wrong token');
    } else {
      printError(e.message);
    }
  }
};

const initCLI = () => {
  const args = getArgs(process.argv);

  if (args.h) {
    return printHelp();
  }

  if (args.s) {
    return saveCity(args.s);
  }

  if (args.t) {
    return saveToken(args.t);
  }

  if (!args.h) {
    return getForecast();
  }
};

initCLI();
